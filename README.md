# Conway's Game of Life

Proof of competence in Android development. Hire me!
gabor.varga.99@gmail.com

This application is a job interview assignment, demonstrating my skills in Android development. 

#### Assignment specification

 * An application that plays Conway's Game of Life.
 * Visuals are irrelevant.
 * Focus on code quality and style.

#### Features

 * DI (Dagger2)
 * MVVM architecture using Android Architecture Componens (ViewModels, LiveData, Data Binding)
 * Business logic driven by RxJava/RxKotlin.
 * Sample Unit Tests (JUnit)
 * Sample UI Tests (Espresso)

#### Design

 * I have put emphasis on clean design, maintainability and testability.
 * Dependencies are injected (using Dagger2), using constructor injection where possible (including the ViewModels).
 * Android dependencies only appear at UI level. Business logic and UI flow (up to and including ViewModels) are clean.
 * Business logic is driven by RxJava/RxKotlin (preferred for its large operator set and for Android independence). At the UI Layer boundary, LiveData bindings are used, for their excellent compatibility with the Android UI.
 * The UI is bound to the ViewModels using the Data Binding Library.

#### Testability

 * Almost all of the application code (business logic, ViewModels, even the UI bindings) are testable with simple Unit Tests. No Android framework classes are needed (Context, UI, etc.)
 * Asynchronous / timed behavior is testable (timing aspect is injected/mockable)

