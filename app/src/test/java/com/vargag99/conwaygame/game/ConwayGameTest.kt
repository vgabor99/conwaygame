package com.vargag99.conwaygame.game

import io.reactivex.observers.TestObserver
import junit.framework.TestCase.assertTrue
import me.carleslc.kotlin.extensions.arrays.matrixOf
import org.junit.Test

/**
 * ConwayGame tests.
 */
class ConwayGameTest {

    @Test
    fun conwayGameBlinker() {
        // Seed the game, do ticks and check the outcome.
        val game = ConwayGame()
        val horizontalLine = matrixOf(3, 3, false).apply {
            this[1][0] = true
            this[1][1] = true
            this[1][2] = true
        }
        val verticalLine = matrixOf(3, 3, false).apply {
            this[0][1] = true
            this[1][1] = true
            this[2][1] = true
        }
        val observer = TestObserver<Grid>()
        game.grid().subscribe(observer)
        // Unseeded game
        observer.assertValueCount(0)
        // Seed the game
        game.seed(horizontalLine)
        observer.assertValueCount(1)
        assertTrue(observer.values()[0] contentDeepEquals horizontalLine)
        // Tick
        game.tick()
        observer.assertValueCount(2)
        assertTrue(observer.values()[1] contentDeepEquals verticalLine)
        // Tick
        game.tick()
        observer.assertValueCount(3)
        assertTrue(observer.values()[2] contentDeepEquals horizontalLine)
    }

    // TODO more

}
