package com.vargag99.conwaygame.ui

import com.vargag99.conwaygame.game.ConwayGame
import com.vargag99.conwaygame.game.Grid
import com.vargag99.conwaygame.testutils.RxImmediateSchedulerRule
import com.vargag99.conwaygame.ui.conwaygame.ConwayGameViewModel
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.PublishSubject
import junit.framework.TestCase
import me.carleslc.kotlin.extensions.arrays.matrixOf
import org.junit.Rule
import org.junit.Test

/**
 * ConwayGameViewModel tests.
 */
class ConwayGameViewModelTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Test
    fun spaceShipTest() {
        val game = ConwayGame()
        val ticker = PublishSubject.create<Unit>()
        val viewModel = ConwayGameViewModel(game, ticker)

        val spaceship0 = matrixOf(15, 15, false).apply {
            this[0][2] = true
            this[1][0] = true
            this[1][2] = true
            this[2][1] = true
            this[2][2] = true
        }

        val spaceship1 = matrixOf(15, 15, false).apply {
            this[0][1] = true
            this[1][2] = true
            this[1][3] = true
            this[2][1] = true
            this[2][2] = true
        }

        val observer = TestObserver<Grid>()
        game.grid().subscribe(observer)

        // Initial state
        observer.assertValueCount(1)
        TestCase.assertTrue(observer.values()[0] contentDeepEquals spaceship0)

        // Tick
        ticker.onNext(Unit)
        observer.assertValueCount(2)
        TestCase.assertTrue(observer.values()[1] contentDeepEquals spaceship1)

        // TODO more
    }

    // TODO more

}
