package com.vargag99.conwaygame

import android.content.pm.ActivityInfo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.vargag99.conwaygame.R.id.action_bar
import com.vargag99.conwaygame.R.id.message
import com.vargag99.conwaygame.ui.ConwayGameActivity
import com.zhuinden.espressohelper.checkHasAnyText
import com.zhuinden.espressohelper.matchView
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * UI test for ConwayGameActivity
 */
@RunWith(AndroidJUnit4::class)
class ConwayGameTest {

    @JvmField
    @field:Rule
    var rule = ActivityTestRule(ConwayGameActivity::class.java)

    @Test
    fun conwayGame() {
        // Check page content
        val activity = rule.activity
        message.checkHasAnyText()
        action_bar.matchView().check(matches(hasDescendant(withText(R.string.app_name))))
        // TODO more
    }

    @Test
    fun orientationChanges() {
        // Check orientation changes
        val activity = rule.activity
        message.checkHasAnyText()
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    // TODO more

}
