package com.vargag99.conwaygame.ui.conwaygame

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.toLiveData
import com.vargag99.conwaygame.game.Grid
import com.vargag99.conwaygame.game.IConwayGame
import com.vargag99.conwaygame.game.Seeds
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Simple view model.
 * Initializes itself with a hard-coded seed.
 * Starts ticking when the data is first accessed.
 * TODO add more functionality like pausing the ticking, accept seed from outside, etc.
 */
class ConwayGameViewModel @Inject constructor(private val game: IConwayGame, private val ticker: Observable<Unit>) :
    ViewModel() {
    private var ticks: Disposable? = null
    private val gridSignal: Flowable<Grid> by lazy {
        // Start ticking when the game is first accessed.
        ticks = ticker
            .subscribe { game.tick() }
        game.grid()
            .toFlowable(BackpressureStrategy.LATEST)
            .observeOn(AndroidSchedulers.mainThread())
    }

    init {
        game.seed(Seeds.spaceship)
    }

    val grid: LiveData<Grid> = gridSignal.toLiveData()

    override fun onCleared() {
        super.onCleared()
        ticks?.dispose()
    }
}
