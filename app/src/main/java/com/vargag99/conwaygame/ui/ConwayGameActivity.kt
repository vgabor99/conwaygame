package com.vargag99.conwaygame.ui

import android.os.Bundle
import com.vargag99.conwaygame.R
import com.vargag99.conwaygame.ui.conwaygame.ConwayGameFragment
import dagger.android.support.DaggerAppCompatActivity

class ConwayGameActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.conway_game_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ConwayGameFragment())
                .commitNow()
        }
    }

}
