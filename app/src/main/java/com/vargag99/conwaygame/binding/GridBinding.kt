package com.vargag99.conwaygame.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.vargag99.conwaygame.game.Grid


/**
 * Simplistic binding (to a text view).
 * Placeholder for binding to a proper graphical view.
 */
@BindingAdapter("grid")
fun setGrid(
    view: TextView,
    grid: Grid?
) {
    val render = grid?.joinToString(separator = "\n") {
        it.joinToString(separator = " ") { if (it) "■" else "□" }
    }
    view.text = render
}