package com.vargag99.conwaygame.di

import com.vargag99.conwaygame.ui.conwaygame.ConwayGameFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeConwayGameFragment(): ConwayGameFragment

    // TODO other fragments here
}