package com.vargag99.conwaygame.di

import com.vargag99.conwaygame.ConwayGameApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityModule::class,
        AppModule::class
    ]
)
@Singleton
internal interface AppComponent : AndroidInjector<ConwayGameApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<ConwayGameApplication>()
}