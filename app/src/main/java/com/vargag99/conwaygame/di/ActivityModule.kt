package com.vargag99.conwaygame.di

import com.vargag99.conwaygame.ui.ConwayGameActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeConwayGameActivity(): ConwayGameActivity

    // TODO other activities here
}