package com.vargag99.conwaygame.di

import dagger.Module

@Module(
    includes = [
        GameModule::class,
        ViewModelModule::class
    ]
)
object AppModule {
}