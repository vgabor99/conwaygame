package com.vargag99.conwaygame.di

import com.vargag99.conwaygame.game.ConwayGame
import com.vargag99.conwaygame.game.IConwayGame
import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class GameModule {
    @Provides
    @Singleton
    internal fun provideGame(): IConwayGame {
        return ConwayGame()
    }

    @Provides
    internal fun provideGameTicker(): Observable<Unit> {
        return io.reactivex.Observable.interval(500, TimeUnit.MILLISECONDS)
            .map { Unit }
    }
}
