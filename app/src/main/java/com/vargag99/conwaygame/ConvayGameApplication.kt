package com.vargag99.conwaygame

import com.vargag99.conwaygame.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication


class ConwayGameApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<ConwayGameApplication> {
        return DaggerAppComponent.builder().create(this)
    }

}