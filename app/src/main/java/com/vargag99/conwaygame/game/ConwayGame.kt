package com.vargag99.conwaygame.game

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import me.carleslc.kotlin.extensions.arrays.columnSize
import me.carleslc.kotlin.extensions.arrays.matrixOf
import me.carleslc.kotlin.extensions.arrays.rowSize

class ConwayGame : IConwayGame {
    private var gridVar: BehaviorSubject<Grid> = BehaviorSubject.create()

    override fun grid(): Observable<Grid> = gridVar

    override fun seed(grid: Grid) {
        gridVar.onNext(grid)
    }

    override fun tick() {
        val cur = gridVar.value ?: return
        gridVar.onNext(computeNext(cur))
    }

    private fun computeNext(cur: Grid): Grid {
        var next = matrixOf(cur.rowSize, cur.columnSize, false)
        for (row in 0..cur.rowSize - 1) {
            for (col in 0..cur.columnSize - 1) {
                next[row][col] = applyRules(cur, row, col)
            }
        }
        return next
    }

    private fun applyRules(grid: Grid, row: Int, col: Int): Boolean {
        val neighbors = grid.countNeighbors(row, col)
        return when (grid[row][col]) {
            // Live cell
            true -> when (neighbors) {
                0, 1 -> false // Die due to underpopulation
                2, 3 -> true // Live on
                else -> false // Die due to overpopulation
            }
            // Dead cell
            false -> when (neighbors) {
                3 -> true // Come alive by reproduction
                else -> false // Stay dead
            }

        }
    }

}