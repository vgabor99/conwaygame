package com.vargag99.conwaygame.game

import io.reactivex.Observable

/**
 * Game model.
 */
interface IConwayGame {
    /**
     * Observable grid.
     * @return Grid.
     */
    fun grid(): Observable<Grid>

    /**
     * Seed the game.
     * @param grid Seed grid.
     */
    fun seed(grid: Grid)

    /**
     * Advance the time with one tick.
     */
    fun tick()
}