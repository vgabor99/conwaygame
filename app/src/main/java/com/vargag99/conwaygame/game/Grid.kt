package com.vargag99.conwaygame.game

import me.carleslc.kotlin.extensions.arrays.Matrix

/**
 * This is simple, but happens to fit our purposes. Self-contained with its size.
 */
typealias Grid = Matrix<Boolean>

/**
 * Count neigbors of a given cell (all 8 directions).
 */
fun Grid.countNeighbors(row: Int, col: Int): Int {
    var count = 0
    for (r in row - 1..row + 1) {
        for (c in col - 1..col + 1) {
            if (row == r && col == c) continue // Exclude own position (not a neighbor)
            this.getOrNull(r)?.getOrNull(c)?.let { if (it) count++ }
        }
    }
    return count
}
