package com.vargag99.conwaygame.game

import me.carleslc.kotlin.extensions.arrays.matrixOf

/**
 * Some seeds to play with.
 */
object Seeds {
    val blinker = matrixOf(3, 3, false).apply {
        this[1][0] = true
        this[1][1] = true
        this[1][2] = true
    }
    val spaceship = matrixOf(15, 15, false).apply {
        this[0][2] = true
        this[1][0] = true
        this[1][2] = true
        this[2][1] = true
        this[2][2] = true
    }
}